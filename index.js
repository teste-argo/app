import registerRootComponent from 'expo/build/launch/registerRootComponent';

import App from './src/ui/AppEntry';
import { gestureHandlerRootHOC } from 'react-native-gesture-handler';

registerRootComponent(gestureHandlerRootHOC(App));
