import { useQuery } from '@tanstack/react-query';
import { BASE_URL } from '@/app/constants';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { UnauthenticatedError } from '@/app/errors/unauthenticated-error';

export const useTodoListIndexQuery = () =>
  useQuery({
    queryKey: ['todoListIndex'],
    queryFn: async () => {
      const value = await AsyncStorage.getItem('@auth/token');
      const request = await fetch(`${BASE_URL}/items`, {
        method: 'get',
        headers: { 'Content-type': 'application/json', Authorization: `Bearer ${value}` },
      });

      if (request.status === 401) {
        return Promise.reject(new UnauthenticatedError());
      }

      const { data } = await request.json();
      return data.items;
    },
  });
