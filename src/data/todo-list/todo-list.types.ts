export type TodoListItem = {
  id: number;
  title: string;
  description: string;
  is_completed: boolean;
};

export type TodoListItemData = {
  title: string;
  description: string;
};

export type TodoListItemUpdateData = TodoListItemData & {
  id: number;
};

export type TodoListItemProps = TodoListItem & {
  onPressIn: () => void;
  onPressOut: () => void;
  isActive: boolean;
};
