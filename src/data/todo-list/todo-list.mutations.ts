import { useMutation } from '@tanstack/react-query';
import { BASE_URL } from '@/app/constants';
import { TodoListItemData, TodoListItemUpdateData } from '@/data/todo-list/todo-list.types';
import AsyncStorage from '@react-native-async-storage/async-storage';

export const useTodoListItemAddMutation = () =>
  useMutation({
    mutationFn: async ({ title, description }: TodoListItemData) => {
      const value = await AsyncStorage.getItem('@auth/token');
      const request = await fetch(`${BASE_URL}/items`, {
        method: 'post',
        headers: { 'Content-type': 'application/json', Authorization: `Bearer ${value}` },
        body: JSON.stringify({ title, description }),
      });

      return request.json();
    },
  });

export const useTodoListItemRemoveMutation = () =>
  useMutation({
    mutationFn: async (id: number) => {
      const value = await AsyncStorage.getItem('@auth/token');
      const request = await fetch(`${BASE_URL}/items/${id}`, {
        method: 'delete',
        headers: { 'Content-type': 'application/json', Authorization: `Bearer ${value}` },
      });

      return request.json();
    },
  });

export const useTodoListReorderMutation = () =>
  useMutation({
    mutationFn: async (itemsOrder: Array<number>) => {
      const value = await AsyncStorage.getItem('@auth/token');
      const request = await fetch(`${BASE_URL}/items/reorder`, {
        method: 'post',
        headers: { 'Content-type': 'application/json', Authorization: `Bearer ${value}` },
        body: JSON.stringify({ items: itemsOrder }),
      });

      return request.json();
    },
  });

export const useTodoListMarkAsCompletedMutation = () =>
  useMutation({
    mutationFn: async (id: number) => {
      const value = await AsyncStorage.getItem('@auth/token');
      const request = await fetch(`${BASE_URL}/items/${id}/markAsCompleted`, {
        method: 'post',
        headers: { 'Content-type': 'application/json', Authorization: `Bearer ${value}` },
      });

      return request.json();
    },
  });

export const useTodoListMarkAsUncompletedMutation = () =>
  useMutation({
    mutationFn: async (id: number) => {
      const value = await AsyncStorage.getItem('@auth/token');
      const request = await fetch(`${BASE_URL}/items/${id}/markAsUncompleted`, {
        method: 'post',
        headers: { 'Content-type': 'application/json', Authorization: `Bearer ${value}` },
      });

      return request.json();
    },
  });

export const useTodoListItemUpdateMutation = () =>
  useMutation({
    mutationFn: async ({ title, description, id }: TodoListItemUpdateData) => {
      const value = await AsyncStorage.getItem('@auth/token');
      const request = await fetch(`${BASE_URL}/items/${id}`, {
        method: 'put',
        headers: { 'Content-type': 'application/json', Authorization: `Bearer ${value}` },
        body: JSON.stringify({ title, description }),
      });

      return request.json();
    },
  });
