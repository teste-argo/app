export type UserEntity = {
  id: number;
  email: string;
  created_at: string;
  updated_at: string;
};

export type UserSignUpData = {
  email: string;
  password: string;
};

export type UserSignInData = UserSignUpData;
