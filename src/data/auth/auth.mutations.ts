import { useMutation } from '@tanstack/react-query';
import { BASE_URL } from '@/app/constants';
import { UserSignInData, UserSignUpData } from '@/data/auth/auth.types';
import { SignUpValidationError } from '@/app/errors/sign-up-validation-error';
import { SignUpError } from '@/app/errors/sign-up-error';
import { SignInError } from '@/app/errors/sign-in-error';

export const useAuthSignUpMutation = () =>
  useMutation({
    mutationFn: async ({ email, password }: UserSignUpData) => {
      const request = await fetch(`${BASE_URL}/auth/signup`, {
        method: 'post',
        headers: { 'Content-type': 'application/json' },
        body: JSON.stringify({ email, password }),
      });

      const json = await request.json();

      if (!request.ok) {
        if (request.status === 422) {
          return Promise.reject(new SignUpValidationError(json.data.all_errors));
        }

        return Promise.reject(new SignUpError());
      }

      return json;
    },
  });

export const useAuthSignInMutation = () =>
  useMutation({
    mutationFn: async ({ email, password }: UserSignInData) => {
      const request = await fetch(`${BASE_URL}/auth/signin`, {
        method: 'post',
        headers: { 'Content-type': 'application/json' },
        body: JSON.stringify({ email, password }),
      });

      const json = await request.json();

      if (!request.ok) {
        return Promise.reject(new SignInError());
      }

      return json;
    },
  });
