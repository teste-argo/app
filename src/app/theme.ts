import { MD3LightTheme } from 'react-native-paper';
export const appTheme = {
  ...MD3LightTheme,
  roundness: 1,
  colors: {
    ...MD3LightTheme.colors,
    primary: '#128c7e',
  },
};
