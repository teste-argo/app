export class SignInError extends Error {
  public constructor() {
    super('Usuário ou senha inválidos.');
  }
}
