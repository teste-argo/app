export class SignUpError extends Error {
  public constructor() {
    super('Falha ao registrar.');
  }
}
