export class SignUpValidationError extends Error {
  public constructor(readonly errors: string[]) {
    super();
  }

  get message() {
    return this.errors.join('\n');
  }
}
