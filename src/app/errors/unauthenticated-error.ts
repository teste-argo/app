export class UnauthenticatedError extends Error {
  public constructor() {
    super('Usuário não autenticado.');
  }
}
