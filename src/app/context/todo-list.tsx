import React, {
  createContext,
  Dispatch,
  PropsWithChildren,
  SetStateAction,
  useEffect,
  useState,
} from 'react';
import { TodoListItem } from '@/data/todo-list/todo-list.types';
import { useTodoListIndexQuery } from '@/data/todo-list/todo-list.queries';

export type TodoListContextProps = {
  setTodoListItems: Dispatch<SetStateAction<TodoListItem[]>>;
  todoListItems: TodoListItem[];
  isLoading: boolean;
  showCompleted: boolean;
  showUncompleted: boolean;
  setCompleted: Dispatch<SetStateAction<boolean>>;
  setUncompleted: Dispatch<SetStateAction<boolean>>;
};

export const TodoListContext = createContext<TodoListContextProps>({} as TodoListContextProps);

export const TodoListProvider: React.FC<PropsWithChildren> = ({ children }) => {
  const { data, isLoading } = useTodoListIndexQuery();
  const [todoListItems, setTodoListItems] = useState([] as TodoListItem[]);
  const [showCompleted, setCompleted] = useState(true);
  const [showUncompleted, setUncompleted] = useState(true);

  useEffect(() => {
    if (data) {
      setTodoListItems(data);
    }
  }, [data]);

  return (
    <TodoListContext.Provider
      value={{
        isLoading,
        todoListItems,
        setTodoListItems,
        showCompleted,
        setCompleted,
        showUncompleted,
        setUncompleted,
      }}
    >
      {children}
    </TodoListContext.Provider>
  );
};
