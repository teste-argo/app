import React, { forwardRef, useImperativeHandle, useState } from 'react';
import { Button, Dialog, Portal, TextInput } from 'react-native-paper';
import { signupStyles } from '@/ui/screens/signup/signup.styles';
import {
  useTodoListItemAddMutation,
  useTodoListItemUpdateMutation,
} from '@/data/todo-list/todo-list.mutations';
import { useToast } from 'react-native-paper-toast';
import { queryClient } from '@/app/query-client';

export type TodoItemUpdateModalRef = {
  open: (title: string, description: string, id: number) => void;
  close: () => void;
};

export const TodoItemUpdateModal = forwardRef<TodoItemUpdateModalRef>((props, ref) => {
  const { mutateAsync } = useTodoListItemUpdateMutation();
  const [open, setOpen] = useState(false);
  const [id, setId] = useState<number | null>(null);
  const [title, setTitle] = useState('');
  const [description, setDescription] = useState('');
  const toaster = useToast();

  const handleFormSubmit = async () => {
    setOpen(false);
    await mutateAsync({
      title,
      description,
      id: id!,
    });
    await queryClient.invalidateQueries({ queryKey: ['todoListIndex'] });
    toaster.show({ message: 'Item alterado com sucesso!', type: 'success' });
  };

  useImperativeHandle(ref, () => ({
    open(title, description, id) {
      setOpen(true);
      setTitle(title);
      setDescription(description);
      setId(id);
    },
    close() {
      setOpen(false);
    },
  }));

  return (
    <Portal>
      <Dialog
        visible={open}
        onDismiss={() => {
          setOpen(false);
        }}
      >
        <Dialog.Title>Editar item</Dialog.Title>
        <Dialog.Content>
          <TextInput
            value={title}
            label="Título"
            style={signupStyles.input}
            onChangeText={(text) => setTitle(text)}
          />
          <TextInput
            value={description}
            label="Descrição"
            multiline
            style={signupStyles.input}
            onChangeText={(text) => setDescription(text)}
          />
        </Dialog.Content>
        <Dialog.Actions>
          <Button
            onPress={() => {
              if (!title) {
                alert('O campo título é obrigatório.');
                return;
              } else if (!description) {
                alert('O campo descrição é obrigatório.');
                return;
              }

              handleFormSubmit();
            }}
          >
            Salvar
          </Button>
          <Button
            onPress={() => {
              setOpen(false);
            }}
          >
            Cancelar
          </Button>
        </Dialog.Actions>
      </Dialog>
    </Portal>
  );
});
