import React, { forwardRef, useImperativeHandle, useState } from 'react';
import { Button, Modal, Portal, Text } from 'react-native-paper';
import { ScrollView, Touchable, TouchableOpacity } from 'react-native';

export type TodoItemInfoModalRef = {
  open: (title: string, description: string) => void;
  close: () => void;
};

export const TodoItemInfoModal = forwardRef<TodoItemInfoModalRef>((props, ref) => {
  const [open, setOpen] = useState(false);
  const [modalTitle, setModalTitle] = useState('');
  const [modalDescription, setModalDescription] = useState('');

  useImperativeHandle(ref, () => ({
    open(title, description) {
      setModalTitle(title);
      setModalDescription(description);
      setOpen(true);
    },
    close() {
      setOpen(false);
    },
  }));

  return (
    <Portal>
      <Modal
        visible={open}
        onDismiss={() => setOpen(false)}
        contentContainerStyle={{ backgroundColor: 'white', padding: 20, margin: 15 }}
      >
        <ScrollView>
          <Text style={{ fontSize: 19, marginBottom: 10 }}>{modalTitle}</Text>
          <Text>{modalDescription}</Text>
          <TouchableOpacity onPress={() => setOpen(false)} style={{ marginTop: 10 }}>
            <Button>Fechar</Button>
          </TouchableOpacity>
        </ScrollView>
      </Modal>
    </Portal>
  );
});
