import React, { forwardRef, useImperativeHandle, useState } from 'react';
import { Button, Dialog, Modal, Portal, Text } from 'react-native-paper';
import { ScrollView, Touchable, TouchableOpacity } from 'react-native';
import { useTodoListItemRemoveMutation } from '@/data/todo-list/todo-list.mutations';
import { useToast } from 'react-native-paper-toast';
import { queryClient } from '@/app/query-client';

export type TodoItemDeleteModalRef = {
  open: (id: number, title: string) => void;
  close: () => void;
};

export const TodoItemDeleteModal = forwardRef<TodoItemDeleteModalRef>((props, ref) => {
  const [open, setOpen] = useState(false);
  const [id, setId] = useState<null | number>(null);
  const [title, setTitle] = useState('');
  const { mutateAsync } = useTodoListItemRemoveMutation();

  useImperativeHandle(ref, () => ({
    open(id, title) {
      setOpen(true);
      setId(id);
      setTitle(title);
    },
    close() {
      setOpen(false);
    },
  }));

  return (
    <Portal>
      <Dialog
        visible={open}
        onDismiss={() => {
          setOpen(false);
        }}
      >
        <Dialog.Title>Confirmação de remoção</Dialog.Title>
        <Dialog.Content>
          <Text variant="bodyMedium">Tem certeza de que deseja remover o item {title}?</Text>
        </Dialog.Content>
        <Dialog.Actions>
          <Button
            onPress={async () => {
              // setOpen(false);
              await mutateAsync(id!);
              await queryClient.invalidateQueries({ queryKey: ['todoListIndex'] });
              // toaster.show({ message: 'Item removido com sucesso!', type: 'success' });
            }}
          >
            Remover
          </Button>
          <Button
            onPress={() => {
              setOpen(false);
            }}
          >
            Cancelar
          </Button>
        </Dialog.Actions>
      </Dialog>
    </Portal>
  );
});
