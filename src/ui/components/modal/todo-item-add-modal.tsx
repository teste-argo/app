import React, { forwardRef, useImperativeHandle, useState } from 'react';
import { Button, Dialog, Portal, TextInput } from 'react-native-paper';
import { signupStyles } from '@/ui/screens/signup/signup.styles';
import { useTodoListItemAddMutation } from '@/data/todo-list/todo-list.mutations';
import { useToast } from 'react-native-paper-toast';
import { queryClient } from '@/app/query-client';

export type TodoItemAddModalRef = {
  open: () => void;
  close: () => void;
};

export const TodoItemAddModal = forwardRef<TodoItemAddModalRef>((props, ref) => {
  const { mutateAsync } = useTodoListItemAddMutation();
  const [open, setOpen] = useState(false);
  const [title, setTitle] = useState('');
  const [description, setDescription] = useState('');
  const toaster = useToast();

  const handleFormSubmit = async () => {
    setOpen(false);
    await mutateAsync({
      title,
      description,
    });
    await queryClient.invalidateQueries({ queryKey: ['todoListIndex'] });
    toaster.show({ message: 'Item adicionado com sucesso!', type: 'success' });
  };

  useImperativeHandle(ref, () => ({
    open() {
      setOpen(true);
      setTitle('');
      setDescription('');
    },
    close() {
      setOpen(false);
    },
  }));

  return (
    <Portal>
      <Dialog
        visible={open}
        onDismiss={() => {
          setOpen(false);
        }}
      >
        <Dialog.Title>Adicionar item</Dialog.Title>
        <Dialog.Content>
          <TextInput
            label="Título"
            style={signupStyles.input}
            onChangeText={(text) => setTitle(text)}
          />
          <TextInput
            label="Descrição"
            multiline
            style={signupStyles.input}
            onChangeText={(text) => setDescription(text)}
          />
        </Dialog.Content>
        <Dialog.Actions>
          <Button
            onPress={() => {
              if (!title) {
                alert('O campo título é obrigatório.');
                return;
              } else if (!description) {
                alert('O campo descrição é obrigatório.');
                return;
              }

              handleFormSubmit();
            }}
          >
            Salvar
          </Button>
          <Button
            onPress={() => {
              setOpen(false);
            }}
          >
            Cancelar
          </Button>
        </Dialog.Actions>
      </Dialog>
    </Portal>
  );
});
