import React, { useEffect, useState } from 'react';
import { RootStack } from '@/app/navigators/root-stack.navigator';
import SigninScreen from '@/ui/screens/signin/signin.screen';
import SignupScreen from '@/ui/screens/signup/signup.screen';
import TodoListScreen from '@/ui/screens/todo-list/todo-list.screen';
import { View } from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { TodoListFilterButtons } from '@/ui/screens/todo-list/components/todo-list-filter-buttons';

export const App: React.FC = () => {
  const [token, setToken] = useState<undefined | null | string>(undefined);

  useEffect(() => {
    (async () => {
      const value = await AsyncStorage.getItem('@auth/token');
      setToken(!value ? null : value);
    })();
  }, []);

  if (token === undefined) {
    return;
  }

  return (
    <RootStack.Navigator initialRouteName={token ? 'TodoList' : 'SignIn'}>
      <RootStack.Screen name="SignIn" component={SigninScreen} options={{ headerShown: false }} />
      <RootStack.Screen
        name="SignUp"
        component={SignupScreen}
        options={{
          headerTransparent: true,
          headerBackTitleVisible: false,
          headerTintColor: 'white',
          headerTitle: 'Registrar',
        }}
      />
      <RootStack.Screen
        name="TodoList"
        component={TodoListScreen}
        options={{
          headerTransparent: true,
          headerBackTitleVisible: false,
          headerTintColor: 'white',
          headerTitle: 'Lista à fazer',
          headerRight: TodoListFilterButtons,
        }}
      />
    </RootStack.Navigator>
  );
};
