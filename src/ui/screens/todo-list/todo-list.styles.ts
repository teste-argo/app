import { StyleSheet } from 'react-native';

export const todoStyles = StyleSheet.create({
  card: {
    position: 'relative',
    alignItems: 'center',
    flexDirection: 'row',
    marginHorizontal: 15,
    padding: 10,
    borderRadius: 5,
    backgroundColor: 'white',
    borderWidth: 1,
    borderColor: '#ddd',
  },
  dragged: {
    opacity: 0.7,
  },
  title: {
    fontSize: 20,
  },
  description: {
    fontSize: 14,
  },
  container: {
    flex: 1,
  },
  fab: {
    position: 'absolute',
    margin: 16,
    right: 0,
    bottom: 0,
  },
  content: {
    flexGrow: 1,
    paddingLeft: 30,
  },
  checkboxContainer: {
    position: 'absolute',
    zIndex: 2,
  },
});
