import React, { useContext } from 'react';
import { IconButton } from 'react-native-paper';
import { TodoListContext } from '@/app/context/todo-list';

export const TodoListFilterButtons: React.FC = () => {
  const { showCompleted, showUncompleted, setCompleted, setUncompleted } =
    useContext(TodoListContext);
  return (
    <>
      <IconButton
        icon="checkbox-marked"
        iconColor={showCompleted ? 'green' : '#aaa'}
        size={20}
        onPress={() => {
          if (showUncompleted) {
            setCompleted(!showCompleted);
          }
        }}
      />
      <IconButton
        icon="checkbox-blank-outline"
        iconColor={showUncompleted ? 'green' : '#aaa'}
        size={20}
        onPress={() => {
          if (showCompleted) {
            setUncompleted(!showUncompleted);
          }
        }}
      />
    </>
  );
};
