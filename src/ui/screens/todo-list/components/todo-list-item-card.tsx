import React, { useContext, useRef } from 'react';
import { Animated, Pressable, Text, TouchableOpacity, View } from 'react-native';
import { todoStyles } from '@/ui/screens/todo-list/todo-list.styles';
import { truncate } from '@/infra/utils';
import { Checkbox } from 'react-native-paper';
import { TodoListContext } from '@/app/context/todo-list';
import {
  TodoItemInfoModal,
  TodoItemInfoModalRef,
} from '@/ui/components/modal/todo-item-info-modal';
import Swipeable from 'react-native-gesture-handler/Swipeable';
import AnimatedInterpolation = Animated.AnimatedInterpolation;
import {
  TodoItemDeleteModal,
  TodoItemDeleteModalRef,
} from '@/ui/components/modal/todo-item-delete-modal';
import { TodoListItemProps } from '@/data/todo-list/todo-list.types';
import {
  useTodoListMarkAsCompletedMutation,
  useTodoListMarkAsUncompletedMutation,
} from '@/data/todo-list/todo-list.mutations';
import {
  TodoItemUpdateModal,
  TodoItemUpdateModalRef,
} from '@/ui/components/modal/todo-item-update-modal';
import AsyncStorage from '@react-native-async-storage/async-storage';

export const TodoListItemCard: React.FC<TodoListItemProps> = React.memo(
  ({ id, title, description, is_completed, onPressIn, onPressOut, isActive }) => {
    const { todoListItems, setTodoListItems } = useContext(TodoListContext);
    const infoRef = useRef<TodoItemInfoModalRef>(null);
    const deleteRef = useRef<TodoItemDeleteModalRef>(null);
    const updateRef = useRef<TodoItemUpdateModalRef>(null);
    const { mutateAsync: markAsCompleted } = useTodoListMarkAsCompletedMutation();
    const { mutateAsync: markAsUncompleted } = useTodoListMarkAsUncompletedMutation();

    const renderLeftActions = (
      progress: AnimatedInterpolation<string | number>,
      dragX: AnimatedInterpolation<string | number>,
    ) => {
      const scale = dragX.interpolate({
        inputRange: [0, 140],
        outputRange: [0, 1],
        extrapolate: 'clamp',
      });
      return (
        <Animated.View
          style={{
            flexDirection: 'row',
            transform: [{ scale }],
          }}
        >
          <TouchableOpacity
            onPress={() => deleteRef.current!.open(id, title)}
            style={{
              width: 80,
              height: '100%',
              backgroundColor: 'red',
              justifyContent: 'center',
              alignItems: 'center',
            }}
          >
            <Animated.Text
              style={[
                {
                  textAlignVertical: 'center',
                  height: '100%',
                  color: '#fff',
                },
              ]}
            >
              Apagar
            </Animated.Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={{
              width: 80,
              height: '100%',
              backgroundColor: '#3C74A8',
              justifyContent: 'center',
              alignItems: 'center',
            }}
            onPress={async () => {
              updateRef.current!.open(title, description, id);
            }}
          >
            <Animated.Text
              style={[
                {
                  textAlignVertical: 'center',
                  height: '100%',
                  color: '#fff',
                },
              ]}
            >
              Editar
            </Animated.Text>
          </TouchableOpacity>
        </Animated.View>
      );
    };

    return (
      <Swipeable renderLeftActions={renderLeftActions}>
        <Pressable
          style={[todoStyles.card, isActive && todoStyles.dragged]}
          onLongPress={onPressIn}
          onPressOut={onPressOut}
          onPress={() => {
            infoRef.current!.open(title, description);
          }}
        >
          <View style={todoStyles.checkboxContainer}>
            <Checkbox
              status={is_completed ? 'checked' : 'unchecked'}
              onPress={() => {
                const todoItemIndex = todoListItems.findIndex((item) => item.id === id);
                todoListItems[todoItemIndex].is_completed =
                  !todoListItems[todoItemIndex].is_completed;
                setTodoListItems([...todoListItems]);

                if (todoListItems[todoItemIndex].is_completed) {
                  markAsCompleted(id);
                } else {
                  markAsUncompleted(id);
                }
              }}
            />
          </View>
          <View style={[todoStyles.content, is_completed ? { opacity: 0.3 } : {}]}>
            <Text
              style={[todoStyles.title, is_completed ? { textDecorationLine: 'line-through' } : {}]}
            >
              {title}
            </Text>
            <Text
              style={[
                todoStyles.description,
                is_completed ? { textDecorationLine: 'line-through' } : {},
              ]}
            >
              {truncate(description, 100)}
            </Text>
          </View>
        </Pressable>
        <TodoItemInfoModal ref={infoRef} />
        <TodoItemDeleteModal ref={deleteRef} />
        <TodoItemUpdateModal ref={updateRef} />
      </Swipeable>
    );
  },
);
