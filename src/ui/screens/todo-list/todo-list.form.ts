import * as yup from 'yup';

export type TodoListForm = {
  title: string;
  description: string;
};

export const todoListSchema = yup
  .object({
    title: yup.string().required(),
    description: yup.string().required().min(10),
  })
  .required();
