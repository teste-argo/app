import React, { useContext, useMemo, useRef } from 'react';
import { ImageBackground, View } from 'react-native';
import { todoStyles } from '@/ui/screens/todo-list/todo-list.styles';
import { FAB, Icon, Text } from 'react-native-paper';
import { useSafeAreaInsets } from 'react-native-safe-area-context';
import { useHeaderHeight } from '@react-navigation/elements';
import { TodoListItemCard } from '@/ui/screens/todo-list/components/todo-list-item-card';
import { TodoListContext } from '@/app/context/todo-list';
import { TodoItemAddModal, TodoItemAddModalRef } from '@/ui/components/modal/todo-item-add-modal';
import { TodoListItem } from '@/data/todo-list/todo-list.types';
import DragList, { DragListRenderItemInfo } from 'react-native-draglist';
import { useTodoListReorderMutation } from '@/data/todo-list/todo-list.mutations';
import { useToast } from 'react-native-paper-toast';

export default function TodoListScreen() {
  const { bottom } = useSafeAreaInsets();
  const headerHeight = useHeaderHeight();
  const { todoListItems, setTodoListItems, showCompleted, showUncompleted } =
    useContext(TodoListContext);
  const addRef = useRef<TodoItemAddModalRef>(null);
  const { mutateAsync: reorder } = useTodoListReorderMutation();
  const toaster = useToast();

  const todoListFiltered = useMemo(() => {
    return todoListItems.filter(
      (item) => (showCompleted && item.is_completed) || (showUncompleted && !item.is_completed),
    );
  }, [todoListItems, showCompleted, showUncompleted]);

  function renderItem(info: DragListRenderItemInfo<TodoListItem>) {
    const { item, onDragStart, onDragEnd, isActive } = info;

    return (
      <TodoListItemCard
        {...item}
        onPressIn={onDragStart}
        onPressOut={onDragEnd}
        isActive={isActive}
      />
    );
  }

  async function onReordered(fromIndex: number, toIndex: number) {
    const copy = [...todoListItems];
    const removed = copy.splice(fromIndex, 1);

    copy.splice(toIndex, 0, removed[0]);
    setTodoListItems(copy);

    const orderItems = copy.map((item) => item.id);
    await reorder(orderItems);
    toaster.show({ message: 'Lista atualizada.', type: 'success' });
  }

  return (
    <ImageBackground
      source={require('@/assets/background.jpg')}
      style={[todoStyles.container, { paddingTop: headerHeight, paddingBottom: bottom }]}
    >
      {todoListFiltered.length === 0 ? (
        <View
          style={{
            flex: 1,
            alignItems: 'center',
            justifyContent: 'center',
          }}
        >
          <Icon source="checkbox-blank-off" color="white" size={45} />
          <Text style={{ color: 'white', marginTop: 20 }}>Nenhum item encontrado.</Text>
        </View>
      ) : (
        <DragList
          data={todoListFiltered}
          keyExtractor={(item: TodoListItem) => `${item.id}`}
          onReordered={onReordered}
          renderItem={renderItem}
          ItemSeparatorComponent={() => <View style={{ height: 4 }} />}
        />
      )}
      <FAB
        icon="plus"
        style={[todoStyles.fab, { bottom }]}
        onPress={() => {
          addRef.current!.open();
        }}
      />
      <TodoItemAddModal ref={addRef} />
    </ImageBackground>
  );
}
