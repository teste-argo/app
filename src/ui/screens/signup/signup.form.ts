import * as yup from 'yup';

export type SignUpForm = {
  email: string;
  password: string;
};

export const signUpSchema = yup
  .object({
    email: yup.string().email().required(),
    password: yup.string().required(),
  })
  .required();
