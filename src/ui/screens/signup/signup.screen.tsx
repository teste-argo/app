import React from 'react';
import { StatusBar } from 'expo-status-bar';
import { ImageBackground, KeyboardAvoidingView, View } from 'react-native';
import { Button, Text, TextInput } from 'react-native-paper';
import { signupStyles } from './signup.styles';
import { Controller, useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import { SignUpForm, signUpSchema } from '@/ui/screens/signup/signup.form';
import { useToast } from 'react-native-paper-toast';
import { useAuthSignUpMutation } from '@/data/auth/auth.mutations';
import { useNavigation } from '@react-navigation/native';
import { NativeStackNavigationProp } from '@react-navigation/native-stack';

export default function SignupScreen() {
  const { replace } = useNavigation<NativeStackNavigationProp<RootStackParamList>>();

  const {
    handleSubmit,
    control,
    formState: { errors },
  } = useForm<SignUpForm>({
    defaultValues: {
      email: '',
      password: '',
    },
    resolver: yupResolver(signUpSchema),
  });

  const { mutateAsync } = useAuthSignUpMutation();

  const toaster = useToast();

  const handleFormSubmit = async ({ email, password }: SignUpForm) => {
    try {
      await mutateAsync({
        email,
        password,
      });
      replace('SignIn');
      toaster.show({ message: 'Conta criada com sucesso.', type: 'success' });
    } catch (e: any) {
      toaster.show({ message: e.message, type: 'error' });
    }
  };

  return (
    <KeyboardAvoidingView style={signupStyles.container} behavior="padding">
      <ImageBackground source={require('@/assets/background.jpg')} style={signupStyles.background}>
        <StatusBar style="light" />
        <Controller
          render={({ formState, field }) => (
            <View>
              <TextInput
                label="Email"
                value={field.value}
                style={signupStyles.input}
                onChangeText={field.onChange}
                error={!!formState.errors.email}
              />
              {!!formState.errors.password && (
                <Text style={signupStyles.errorHint}>{formState.errors.email?.message}</Text>
              )}
            </View>
          )}
          name="email"
          control={control}
        />
        <Controller
          render={({ formState, field }) => (
            <View>
              <TextInput
                label="Senha"
                value={field.value}
                style={signupStyles.input}
                onChangeText={field.onChange}
                error={!!formState.errors.password}
                secureTextEntry
              />
              {!!formState.errors.password && (
                <Text style={signupStyles.errorHint}>{formState.errors.password?.message}</Text>
              )}
            </View>
          )}
          name="password"
          control={control}
        />
        <Button
          mode="contained"
          style={signupStyles.input}
          onPress={handleSubmit(handleFormSubmit)}
          // onPress={handleFormSubmit}
        >
          Registrar
        </Button>
      </ImageBackground>
    </KeyboardAvoidingView>
  );
}
