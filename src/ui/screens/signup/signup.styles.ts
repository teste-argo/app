import { StyleSheet } from 'react-native';

export const signupStyles = StyleSheet.create({
  container: {
    flex: 1,
  },
  background: {
    flex: 1,
    paddingHorizontal: 30,
    backgroundColor: '#fff',
    justifyContent: 'center',
  },
  input: {
    marginVertical: 10,
  },
  createAccountActionText: {
    color: 'white',
    textAlign: 'center',
    textDecorationLine: 'underline',
  },
  createAccountAction: {
    paddingVertical: 7,
  },
  errorHint: {
    color: 'red',
  },
});
