import React from 'react';
import { StatusBar } from 'expo-status-bar';
import { ImageBackground, KeyboardAvoidingView, TouchableOpacity, View } from 'react-native';
import { Button, Text, TextInput } from 'react-native-paper';
import { signinStyles } from './signin.styles';
import { Controller, useForm } from 'react-hook-form';
import { SignInForm, signInSchema } from '@/ui/screens/signin/signin.form';
import { yupResolver } from '@hookform/resolvers/yup';
import { useNavigation } from '@react-navigation/native';
import { NativeStackNavigationProp } from '@react-navigation/native-stack';
import { useToast } from 'react-native-paper-toast';
import { useAuthSignInMutation } from '@/data/auth/auth.mutations';
import AsyncStorage from '@react-native-async-storage/async-storage';

export default function SigninScreen() {
  const { replace } = useNavigation<NativeStackNavigationProp<RootStackParamList>>();
  const { navigate } = useNavigation<NativeStackNavigationProp<RootStackParamList>>();
  const toaster = useToast();
  const { mutateAsync } = useAuthSignInMutation();

  const {
    handleSubmit,
    control,
    formState: { errors },
  } = useForm<SignInForm>({
    defaultValues: {
      email: '',
      password: '',
    },
    resolver: yupResolver(signInSchema),
  });

  const handleFormSubmit = async ({ email, password }: SignInForm) => {
    try {
      const result = await mutateAsync({
        email,
        password,
      });
      replace('TodoList');
      toaster.show({ message: 'Login realizado com sucesso.', type: 'success' });
      await AsyncStorage.setItem('@auth/token', result.data.token);
    } catch (e: any) {
      toaster.show({ message: e.message, type: 'error' });
    }
  };

  return (
    <KeyboardAvoidingView style={signinStyles.container} behavior="padding">
      <ImageBackground source={require('@/assets/background.jpg')} style={signinStyles.background}>
        <StatusBar style="light" />
        <Controller
          render={({ formState, field }) => (
            <View>
              <TextInput
                label="Email"
                value={field.value}
                style={signinStyles.input}
                onChangeText={field.onChange}
                error={!!formState.errors.email}
              />
              {!!formState.errors.email && (
                <Text style={signinStyles.errorHint}>{formState.errors.email?.message}</Text>
              )}
            </View>
          )}
          name="email"
          control={control}
        />
        <Controller
          render={({ formState, field }) => (
            <View>
              <TextInput
                label="Senha"
                value={field.value}
                style={signinStyles.input}
                onChangeText={field.onChange}
                error={!!formState.errors.password}
                secureTextEntry
              />
              {!!formState.errors.password && (
                <Text style={signinStyles.errorHint}>{formState.errors.password?.message}</Text>
              )}
            </View>
          )}
          name="password"
          control={control}
        />
        <Button
          mode="contained"
          style={signinStyles.input}
          onPress={handleSubmit(handleFormSubmit)}
        >
          Entrar
        </Button>
        <TouchableOpacity
          style={signinStyles.createAccountAction}
          onPress={() => navigate('SignUp')}
        >
          <Text style={signinStyles.createAccountActionText}>Criar conta</Text>
        </TouchableOpacity>
      </ImageBackground>
    </KeyboardAvoidingView>
  );
}
