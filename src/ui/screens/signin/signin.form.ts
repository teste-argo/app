import * as yup from 'yup';

export type SignInForm = {
  email: string;
  password: string;
};

export const signInSchema = yup
  .object({
    email: yup.string().email().required(),
    password: yup.string().required(),
  })
  .required();
