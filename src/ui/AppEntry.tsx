import React, { useEffect, useState } from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { RootStack } from '@/app/navigators/root-stack.navigator';
import SigninScreen from '@/ui/screens/signin/signin.screen';
import { PaperProvider } from 'react-native-paper';
import { appTheme } from '@/app/theme';
import SignupScreen from '@/ui/screens/signup/signup.screen';
import TodoListScreen from '@/ui/screens/todo-list/todo-list.screen';
import { RecoilRoot } from 'recoil';
import { TodoListProvider } from '@/app/context/todo-list';
import { ToastProvider } from 'react-native-paper-toast';
import { QueryClientProvider } from '@tanstack/react-query';
import { queryClient } from '@/app/query-client';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { View } from 'react-native';
import { App } from '@/ui/screens/App';

export default function AppEntry() {
  return (
    <RecoilRoot>
      <QueryClientProvider client={queryClient}>
        <PaperProvider theme={appTheme}>
          <ToastProvider>
            <TodoListProvider>
              <NavigationContainer>
                <App />
              </NavigationContainer>
            </TodoListProvider>
          </ToastProvider>
        </PaperProvider>
      </QueryClientProvider>
    </RecoilRoot>
  );
}
