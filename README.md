# Teste Argo

## Aplicativo React Native
O aplicativo dispõe de um sistema de autenticação. Embora não tenha sido requisitado pelo teste, achei de bom tom autenticar o usuário antes de poder preenchar a lista de fazeres.

### Passo a passo
Primeiramente faça o registro clicando no botão Criar Conta.

Após essa etapa da criação da conta, logar com o email e senha do passo anterior.

Para criar um novo item clique no botão FAB inferior direito, insira um título e uma descrição.

Para apagar ou editar, arraste o card do item para a direita e as opções aparecerão.

Para alterar a ordem, pressione por alguns instantes (clique longo) e arraste para reordenar sua lista.

O aplicativo dispõe também de um filtro para items finalizados ou em andamento. Apenas marque e desmarque os botões no canto superior direito.

